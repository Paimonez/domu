using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoloscopeLuck : MonoBehaviour
{
            [SerializeField] private bool[] LuckCheck1;
             [SerializeField] private bool[] LuckCheck2;
             [SerializeField] private bool[] LuckCheck3;
         
             [SerializeField] private GameObject[] LuckImage1;
             [SerializeField] private GameObject[] LuckImage2;
             [SerializeField] private GameObject[] LuckImage3;
         
              public GameObject[] LuckButton1;
              public GameObject[] LuckButton2;
              public GameObject[] LuckButton3;
         
              [SerializeField] private GameObject ShowButton;
         
             public GameObject[] HiddenButtons;
            [SerializeField] private  GameObject nextButton;
             
             private bool nextButtonClicked = false;
             private bool secondNextClicked;
             private bool thirdNextClicked;

             public static List<string> Holo = new List<string>();
             
             void Awake()
             {
                 string selectedHolo = PlayerPrefs.GetString("SelectedHolo", "");
                 if (!string.IsNullOrEmpty(selectedHolo))
                 {
                     Holo.Add(selectedHolo);
                 }
                 PlayerPrefs.DeleteKey("SelectedHolo");
                 PlayerPrefs.Save();

                 secondNextClicked = false;
                 thirdNextClicked = false;
             }
             
             /*public void One(int buttonIndex)
             {
                 if (buttonIndex >= 0 && buttonIndex < LuckCheck1.Length)
                 {
                     ToggleLuck(buttonIndex, LuckCheck1, LuckImage1, LuckButton1, "Luck1");
                 }
         
                 if (buttonIndex >= 0 && buttonIndex < LuckCheck1.Length)
                 {
                     ToggleLuck(buttonIndex, LuckCheck1, LuckImage1, LuckButton1, "Luck2");
                 }
         
                 if (buttonIndex >= 0 && buttonIndex < LuckCheck1.Length)
                 {
                     ToggleLuck(buttonIndex, LuckCheck1, LuckImage1, LuckButton1, "Luck3");
                 }
             }
         
             public void Two(int buttonIndex)
             {
                 if (buttonIndex >= 0 && buttonIndex < LuckCheck2.Length)
                 {
                     ToggleLuck(buttonIndex, LuckCheck2, LuckImage2, LuckButton2, "Luck11");
                 }
         
                 if (buttonIndex >= 0 && buttonIndex < LuckCheck2.Length)
                 {
                     ToggleLuck(buttonIndex, LuckCheck2, LuckImage2, LuckButton2, "Luck22");
                 }
         
                 if (buttonIndex >= 0 && buttonIndex < LuckCheck2.Length)
                 {
                     ToggleLuck(buttonIndex, LuckCheck2, LuckImage2, LuckButton2, "Luck33");
                 }
             }
             
             public void Three(int buttonIndex)
             {
                 if (buttonIndex >= 0 && buttonIndex < LuckCheck3.Length)
                 {
                     ToggleLuck(buttonIndex, LuckCheck3, LuckImage3, LuckButton3, "Luck111");
                 }
         
                 if (buttonIndex >= 0 && buttonIndex < LuckCheck3.Length)
                 {
                     ToggleLuck(buttonIndex, LuckCheck3, LuckImage3, LuckButton3, "Luck222");
                 }
         
                 if (buttonIndex >= 0 && buttonIndex < LuckCheck3.Length)
                 {
                     ToggleLuck(buttonIndex, LuckCheck3, LuckImage3, LuckButton3, "Luck333");
                 }
             }
             */
             
             /////////////////////////////////////////////////////////////////////////////////////////////
             /// ช้อยที่ 1
            public void ChoiceOne(int buttonIndex)
             {
                 ToggleLuck(buttonIndex, LuckCheck1, LuckImage1, LuckButton1, "Luck1");
             }

             public void ChoiceTwo(int buttonIndex)
             {
                 ToggleLuck(buttonIndex, LuckCheck1, LuckImage1, LuckButton1, "Luck2");
             }

             public void ChoiceThree(int buttonIndex)
             {
                 ToggleLuck(buttonIndex, LuckCheck1, LuckImage1, LuckButton1, "Luck3");
             }
            
             
            /// ช้อยที่ 2
             public void ChoiceOne_One(int buttonIndex)
             {
                 ToggleLuck(buttonIndex, LuckCheck2, LuckImage2, LuckButton2, "Luck11");
             }

             public void ChoiceTwo_Two(int buttonIndex)
             {
                 ToggleLuck(buttonIndex, LuckCheck2, LuckImage2, LuckButton2, "Luck22");
             }

             public void ChoiceThree_Three(int buttonIndex)
             {
                 ToggleLuck(buttonIndex, LuckCheck2, LuckImage2, LuckButton2, "Luck33");
             }

             /// ช้อยที่ 3
             public void ChoiceOne_One_One(int buttonIndex)
             {
                 ToggleLuck(buttonIndex, LuckCheck3, LuckImage3, LuckButton3, "Luck111");
             }

             public void ChoiceTwo_Two_Two(int buttonIndex)
             {
                 ToggleLuck(buttonIndex, LuckCheck3, LuckImage3, LuckButton3, "Luck222");
             }

             public void ChoiceThree_Three_Three(int buttonIndex)
             {
                 ToggleLuck(buttonIndex, LuckCheck3, LuckImage3, LuckButton3, "Luck333");
             }
             /////////////////////////////////////////////////////////////////////////////////////////////
             
         
           private void ToggleLuck(int buttonIndex, bool[] luckChecks, GameObject[] luckImages, GameObject[] luckButtons, string holoString)
             {
                 if (buttonIndex >= 0 && buttonIndex < luckChecks.Length && buttonIndex < luckImages.Length && buttonIndex < luckButtons.Length)
                 {
                     if (!luckChecks[buttonIndex])
                     {
                         luckChecks[buttonIndex] = true;
                         luckImages[buttonIndex].SetActive(true);
                         luckButtons[buttonIndex].SetActive(true);
                         Holo.Add(holoString);
                     }
                     else
                     {
                         luckChecks[buttonIndex] = false;
                         luckImages[buttonIndex].SetActive(false);
                         Holo.Remove(holoString); 
                     }
                 }
                 else
                 {
                     Debug.LogError("Invalid buttonIndex or array length.");
                 }
             }
        
       
           
             public void Next()
             {
                 nextButtonClicked = true;
         
                 foreach (GameObject hiddenButton in HiddenButtons)
                 {
                     hiddenButton.SetActive(true);
                 }
         
                 foreach (GameObject luckButton in LuckButton1)
                 {
                     luckButton.SetActive(false);
                 }
         
                 if (!secondNextClicked)
                 {
                     foreach (GameObject luckButton in LuckButton2)
                     {
                         luckButton.SetActive(true);
                     }
         
                     secondNextClicked = true;
                 }
                 else if (!thirdNextClicked) 
                 {
                     foreach (GameObject luckButton in LuckButton2)
                     {
                         luckButton.SetActive(false);
                     }
         
                     foreach (GameObject luckButton in LuckButton3)
                     {
                         luckButton.SetActive(true);
                     }
         
                     thirdNextClicked = true;
                 }
                 else 
                 {
                     foreach (GameObject luckImage in LuckImage1)
                     {
                         luckImage.SetActive(false);
                     }
                     foreach (GameObject luckImage in LuckImage2)
                     {
                         luckImage.SetActive(false);
                     }
                     foreach (GameObject luckImage in LuckImage3)
                     {
                         luckImage.SetActive(false);
                     }
                     foreach (GameObject luckButton in LuckButton3)
                     {
                         luckButton.SetActive(false);
                     }
                     
                     nextButton.SetActive(false);
                     ShowButton.SetActive(true); 
                 }
             }
             
             public void Show()
             {
                 string result = string.Join(" ", Holo);
                 Debug.Log(result);
             }
        }
    
