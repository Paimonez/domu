using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenController : MonoBehaviour
{
    [SerializeField] private bool luckSelected = true;
    [SerializeField] private bool healthSelected ;
    [SerializeField] private bool loveSelected ;

    public GameObject luckScene;
    public GameObject healthScene;
    public GameObject loveScene;

    public void boolCheck()
    {
        if (luckSelected == false)
        {
            luckScene.SetActive(true);
            healthScene.SetActive(false);
            loveScene.SetActive(false);
        }
        else if (healthSelected == false)
        {
            healthScene.SetActive(true);
            luckScene.SetActive(false);
            loveScene.SetActive(false);
        }
        else if (loveSelected == false)
        {
            luckScene.SetActive(false);
            healthScene.SetActive(false);
            loveScene.SetActive(true);
        }
    }
    
    
    public void Next()
    {
            SceneManager.LoadScene(2);
    }
    
    public void NextToChoice()
    {
        if (HoloscopeController.LuckCheck)
        { 
            SceneManager.LoadScene("LuckScene");
         //   PlayerPrefs.SetString("SelectedHolo", "Luck");
          //  PlayerPrefs.Save();
        }
        else if (HoloscopeController.HealthCheck)
        { 
            SceneManager.LoadScene("HealthScene");
          //  PlayerPrefs.SetString("SelectedHolo", "Healthy");
          //  PlayerPrefs.Save();
        }
        else if (HoloscopeController.LoveCheck)
        {
            SceneManager.LoadScene("LoveScene");
         //   PlayerPrefs.SetString("SelectedHolo", "Love" );
           // PlayerPrefs.Save();
        }
    }
    
    public void Show()
    {
        HoloscopeController.Show();
    }
}
