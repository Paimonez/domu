using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayOfWeekCalculate : MonoBehaviour
{
    public Birthday birthdayScript;
    
    public string Calculate(int day, int month, int year)
    {

        if (month < 3)
        {
            month += 12;
            year--;
        }

        int k = year % 100;
        int j = year / 100;

        int dayOfWeek = (day + 13 * (month + 1) / 5 + k + k / 4 + j / 4 + 5 * j) % 7;

        string[] daysOfWeek = { "Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday" };

        return daysOfWeek[dayOfWeek];
    }

    // Call this method to calculate the day of the week using values from Birthday script
    public void CalculateDayOfWeekFromBirthday()
    {
        if (birthdayScript != null)
        {
            int day = birthdayScript.day;
            int month = birthdayScript.month;
            int year = birthdayScript.year;

            string dayOfWeek = Calculate(day, month, year);
            
           // Debug.Log($"{dayOfWeek}");
           HoloscopeController.Holo.Add(dayOfWeek);
        }
        else
        {
            Debug.LogError("Birthday script reference is missing!");
        }
    }
}
