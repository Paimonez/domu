using System;
using UnityEngine;
using UnityEngine.UI;

public class Birthday : MonoBehaviour
{
    [SerializeField] private Dropdown dropdownDay;
    [SerializeField] private Dropdown dropdownMonth;
    [SerializeField] private Dropdown dropdownYear;

    private string selectedTextd;
    private string selectedTextm;
    private string selectedTexty;

    public int day;
    public int month;
    public int year;

    private void Start()
    {
        OnDropdownValueChangedd(0);
        OnDropdownValueChangedm(0);
        OnDropdownValueChangedy(0);
        
        dropdownDay.onValueChanged.AddListener(OnDropdownValueChangedd);
        dropdownMonth.onValueChanged.AddListener(OnDropdownValueChangedm);
        dropdownYear.onValueChanged.AddListener(OnDropdownValueChangedy);

    }
    
    private void OnDropdownValueChangedd(int index)
    {
        selectedTextd = dropdownDay.options[index].text;
        day = index + 1;
    }
    private void OnDropdownValueChangedm(int index)
    {
        selectedTextm = dropdownMonth.options[index].text;
        month = index + 1;
    }private void OnDropdownValueChangedy(int index)
    {
        selectedTexty = dropdownYear.options[index].text;
        int.TryParse(selectedTexty, out year);
    }

    string GetZodiacSign(int month, int day)
    {
        if ((month == 3 && day >= 21) || (month == 4 && day <= 19))
        {
            return "Aries";
        }
        else if ((month == 4 && day >= 20) || (month == 5 && day <= 20))
        {
            return "Taurus";
        }
        else if ((month == 5 && day >= 21) || (month == 6 && day <= 20))
        {
            return "Gemini";
        }
        else if ((month == 6 && day >= 21) || (month == 7 && day <= 22))
        {
            return "Cancer";
        }
        else if ((month == 7 && day >= 23) || (month == 8 && day <= 22))
        {
            return "Leo";
        }
        else if ((month == 8 && day >= 23) || (month == 9 && day <= 22))
        {
            return "Virgo";
        }
        else if ((month == 9 && day >= 23) || (month == 10 && day <= 22))
        {
            return "Libra";
        }
        else if ((month == 10 && day >= 23) || (month == 11 && day <= 21))
        {
            return "Scorpio";
        }
        else if ((month == 11 && day >= 22) || (month == 12 && day <= 21))
        {
            return "Sagittarius";
        }
        else if ((month == 12 && day >= 22) || (month == 1 && day <= 19))
        {
            return "Capricorn";
        }
        else if ((month == 1 && day >= 20) || (month == 2 && day <= 18))
        {
            return "Aquarius";
        }
        else if ((month == 2 && day >= 19) || (month == 3 && day <= 20))
        {
            return "Pisces";
        }
    
        // Default case if no match is found.
        return "Unknown";
    }

    public void ShowZodiacAndBirthday()
    {
        Debug.Log($"{selectedTextd} {selectedTextm} {selectedTexty}");
        string n = GetZodiacSign(month, day);
        HoloscopeController.Holo.Add(n);
    }
}
