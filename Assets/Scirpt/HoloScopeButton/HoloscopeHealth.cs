using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoloscopeHealth : MonoBehaviour
{
    [SerializeField] private bool[] oneCheck;
    [SerializeField] private bool[] twoCheck;
    [SerializeField] private bool[] threeCheck;

    [SerializeField] private GameObject[] oneImage;
    [SerializeField] private GameObject[] twoImage;
    [SerializeField] private GameObject[] threeImage;
    
    
    [SerializeField] private GameObject healthAll1;
    [SerializeField] private GameObject healthAll2;
    [SerializeField] private GameObject healthAll3;
    [SerializeField] private GameObject nextButton;
    [SerializeField] private GameObject showButton;

    [SerializeField] private int step = 1;
    [SerializeField] int index;


    private void Start()
    {
        healthAll2.SetActive(false);
        healthAll3.SetActive(false);
        showButton.SetActive(false);
        index = 0;
    }
    
    
    public void One()
    {
        if (!oneCheck[index])
        {
            oneCheck[index] = true;
            twoCheck[index] = false;
            threeCheck[index] = false;
            oneImage[index].SetActive(true);
            twoImage[index].SetActive(false);
            threeImage[index].SetActive(false);
            if (step == 1)
            {
                HoloscopeController.Holo.Add("1Health");
                HoloscopeController.Holo.Remove("2Health");
                HoloscopeController.Holo.Remove("3Health");
            }
            else if (step == 2)
            {
                HoloscopeController.Holo.Add("11Health");
                HoloscopeController.Holo.Remove("22Health");
                HoloscopeController.Holo.Remove("33Health");
            }
            else if (step == 3)
            {
                HoloscopeController.Holo.Add("111Health");
                HoloscopeController.Holo.Remove("222Health");
                HoloscopeController.Holo.Remove("333Health");
            }
        }
        else
        {
            oneCheck[index] = false;
            oneImage[index].SetActive(false);
            if (step == 1)
            {
                HoloscopeController.Holo.Remove("1Health");
            }
            else if (step == 2)
            {
                HoloscopeController.Holo.Remove("11Health");
            }
            else if (step == 3)
            {
                HoloscopeController.Holo.Remove("111Health");
            }
        }
    }

    public void Two()
    {
        if (!twoCheck[index])
        {
            oneCheck[index] = false;
            twoCheck[index] = true;
            threeCheck[index] = false;
            oneImage[index].SetActive(false);
            twoImage[index].SetActive(true);
            threeImage[index].SetActive(false);
            if (step == 1)
            {
                HoloscopeController.Holo.Add("2Health");
                HoloscopeController.Holo.Remove("1Health");
                HoloscopeController.Holo.Remove("3Health");
            }
            else if (step == 2)
            {
                HoloscopeController.Holo.Add("22Health");
                HoloscopeController.Holo.Remove("11Health");
                HoloscopeController.Holo.Remove("33Health");
            }
            else if (step == 3)
            {
                HoloscopeController.Holo.Add("222Health");
                HoloscopeController.Holo.Remove("111Health");
                HoloscopeController.Holo.Remove("333Health");
            }
        }
        else
        {
            twoCheck[index] = false;
            twoImage[index].SetActive(false);
            if (step == 1)
            {
                HoloscopeController.Holo.Remove("2Health");
            }
            else if (step == 2)
            {
                HoloscopeController.Holo.Remove("22Health");
            }
            else if (step == 3)
            {
                HoloscopeController.Holo.Remove("222Health");
            }
        }
    }

   public void Three()
    {
        if (!threeCheck[index])
        {
            oneCheck[index] = false;
            twoCheck[index] = false;
            threeCheck[index] = true;
            oneImage[index].SetActive(false);              
            twoImage[index].SetActive(false); 
            threeImage[index].SetActive(true);
            if (step == 1)
            {
                HoloscopeController.Holo.Add("3Health");
                HoloscopeController.Holo.Remove("1Health");
                HoloscopeController.Holo.Remove("2Health");
            }
            else if (step == 2)
            {
                HoloscopeController.Holo.Add("33Health");
                HoloscopeController.Holo.Remove("11Health");
                HoloscopeController.Holo.Remove("22Health");
            }
            else if (step == 3)
            {
                HoloscopeController.Holo.Add("333Health");
                HoloscopeController.Holo.Remove("111Health");
                HoloscopeController.Holo.Remove("222Health");
            }
        }
        else
        {
            threeCheck[index] = false;
            threeImage[index].SetActive(false);
            if (step == 1)
            {
                HoloscopeController.Holo.Remove("3Health");
            }
            else if (step == 2)
            {
                HoloscopeController.Holo.Remove("33Health");
            }
            else if (step == 3)
            {
                HoloscopeController.Holo.Remove("333Health");
            }
        }
    }

    public void Next()
    {
        if (step == 1)
        {
            healthAll1.SetActive(false);
            healthAll2.SetActive(true);
            step++;
            index++;
        }
        else if (step == 2)
        {
            healthAll2.SetActive(false);
            healthAll3.SetActive(true);
            step++;
            index++;
        }
        else if (step == 3)
        {
            healthAll3.SetActive(false);
            nextButton.SetActive(false);
            showButton.SetActive(true);
        }
    }
    public void Show()
    {
        HoloscopeController.Show();
    }
}
