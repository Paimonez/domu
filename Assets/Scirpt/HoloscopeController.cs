using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HoloscopeController : MonoBehaviour
{
    [SerializeField] public static bool LuckCheck;
    [SerializeField] public static bool HealthCheck;
    [SerializeField] public static bool LoveCheck;

    [SerializeField] private GameObject LuckImage;
    [SerializeField] private GameObject HealthImage;
    [SerializeField] private GameObject LoveImage;
    
    [SerializeField] private GameObject LuckButton;
    [SerializeField] private GameObject HealthButton;
    [SerializeField] private GameObject LoveButton;
    
    public GameObject[] HiddenButtons;
   
    public GameObject[] HiddenButtonsLuck;
    public GameObject[] HiddenButtonsHealty;
    public GameObject[] HiddenButtonsLove;
    
    bool previousSceneValue = true;

    public static List<string> Holo = new List<string>();

    private void Start()
    {
        LuckCheck = false;
        HealthCheck = false;
        LoveCheck = false;
    }

    public void One()
    {
        if (!LuckCheck)
        {
            LuckCheck = true;
            HealthCheck = false;
            LoveCheck = false;
            LuckImage.SetActive(true);
            HealthImage.SetActive(false);
            LoveImage.SetActive(false);
            Holo.Clear(); // Clear all previous selections
            Holo.Add("Luck");
        }
        else
        {
            LuckCheck = false;
            LuckImage.SetActive(false);
            Holo.Remove("Luck");
        }
    }

    public void Two()
    {
        if (!HealthCheck)
        {
            HealthCheck = true;
            LuckCheck = false;
            LoveCheck = false;
            HealthImage.SetActive(true);
            LuckImage.SetActive(false);
            LoveImage.SetActive(false);
            Holo.Clear(); // Clear all previous selections
            Holo.Add("Healthy");
        }
        else
        {
            HealthCheck = false;
            HealthImage.SetActive(false);
            Holo.Remove("Healthy");
        }
    }

    public void Three()
    {
        if (!LoveCheck)
        {
            LoveCheck = true;
            LuckCheck = false;
            HealthCheck = false;
            LoveImage.SetActive(true);
            LuckImage.SetActive(false);
            HealthImage.SetActive(false);
            Holo.Clear(); // Clear all previous selections
            Holo.Add("Love");
        }
        else
        {
            LoveCheck = false;
            LoveImage.SetActive(false);
            Holo.Remove("Love");
        }
    }
    
    
    public void Next()
    {
        
        foreach (GameObject hiddenButton in HiddenButtons)
        {
            hiddenButton.SetActive(true);
        }
        
        if (LuckCheck)
        {
            foreach (GameObject hiddenButton in HiddenButtonsLuck)
            {
                hiddenButton.SetActive(true);
             
                LuckButton.SetActive(false);
                HealthButton.SetActive(false);
                LoveButton.SetActive(false);
            }
        }
        else if (HealthCheck)
        {
            foreach (GameObject hiddenButton in HiddenButtonsHealty)
            {
                hiddenButton.SetActive(true);
               
                LuckButton.SetActive(false);
                HealthButton.SetActive(false);
                LoveButton.SetActive(false);
            }
        }
        else if (LoveCheck)
        {
            foreach (GameObject hiddenButton in HiddenButtonsLove)
            {
                hiddenButton.SetActive(true);
                
                LuckButton.SetActive(false);
                HealthButton.SetActive(false);
                LoveButton.SetActive(false);
            }
        }
    }
    

    public static void Show()
    {
        string result = string.Join(" ", Holo);
   //     PlayerPrefs.SetString("SelectedHolo", " ");
   //     PlayerPrefs.Save();
        Debug.Log(result);
    }
}
