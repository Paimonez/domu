using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace OpenAI
{
    public class DallE : MonoBehaviour
    {

        //system
        [SerializeField] private GameObject Show;
        [SerializeField] private GameObject Show2;
        [SerializeField] private GameObject Show3;

        // [SerializeField] private InputField inputField;
        [SerializeField] private Button button;
        
        [SerializeField] private Image image;
        [SerializeField] private Image image2;
        [SerializeField] private Image image3;
      
        [SerializeField] private GameObject loadingLabel;

        [SerializeField] private Button showButton;
        [SerializeField] private Button downloadButton;

        private OpenAIApi openai = new OpenAIApi();
        
        
        private void Start()
        {
            showButton.onClick.AddListener(ShowImage);
            downloadButton.onClick.AddListener(DownloadGeneratedImage);
            
            button.onClick.AddListener(SendImageRequest);
          
            image.enabled = false;
          
            downloadButton.onClick.AddListener(DownloadGeneratedImage);
            downloadButton.gameObject.SetActive(false);
        }



        private async void SendImageRequest()
        {
            image.sprite = null;
            button.enabled = false;
            //inputField.enabled = false;
            loadingLabel.SetActive(true);
            

            var response = await openai.CreateImage(new CreateImageRequest
            {
                Prompt = Show.tag,
                //Prompt = inputField.text,
                Size = ImageSize.Size256
            });
            
            var response2 = await openai.CreateImage(new CreateImageRequest
            {
                Prompt = Show2.tag,
                Size = ImageSize.Size256
            });

            var response3 = await openai.CreateImage(new CreateImageRequest
            {
                Prompt = Show3.tag,
                Size = ImageSize.Size256
            });

            if (response.Data != null && response.Data.Count > 0)
            {
                using (var request = new UnityWebRequest(response.Data[0].Url))
                {
                    request.downloadHandler = new DownloadHandlerBuffer();
                    request.SetRequestHeader("Access-Control-Allow-Origin", "*");
                    request.SendWebRequest();

                    while (!request.isDone) await Task.Yield();

                    image.enabled = true;

                    Texture2D texture = new Texture2D(2, 2);
                    texture.LoadImage(request.downloadHandler.data);
                    var sprite = Sprite.Create(texture, new Rect(0, 0, 256, 256), Vector2.zero, 1f);
                    image.sprite = sprite;
                }
            }
            else
            {
                Debug.LogWarning("No image was created from this prompt.");
            }

            button.enabled = true;
            //inputField.enabled = true;
            Show.SetActive(true);
            loadingLabel.SetActive(false);
        }
        
        private void ShowImage()
        {
            showButton.gameObject.SetActive(false);
            downloadButton.gameObject.SetActive(true);
        }

        private void DownloadGeneratedImage()
        {
            if (image.sprite != null)
            {
                byte[] imageBytes = image.sprite.texture.EncodeToPNG();
                File.WriteAllBytes(Application.persistentDataPath + "/generated_image.png", imageBytes);
                Debug.Log("รูปภาพถูกดาวน์โหลดไปยัง: " + Application.persistentDataPath + "/generated_image.png");
            }
            else
            {
                Debug.LogWarning("ยังไม่มีรูปภาพที่ถูกสร้างขึ้นเพื่อดาวน์โหลด");
            }
            
            /* เมื่อคลิกปุ่มดาวน์โหลด ซ่อนปุ่ม "Download" อีกครั้ง
            downloadButton.gameObject.SetActive(false);
            // แสดงปุ่ม "Show" เพื่อให้ผู้เล่นสามารถดูรูปภาพอีกครั้ง
            showButton.gameObject.SetActive(true);*/

        }
    }
}
